package dbinit

import (
	"vst/db"
	"vst/model"
)

//Migrate выполняет миграции
func Migrate() {
	c := db.GetDB()
	c.Debug().AutoMigrate(db.GetAllModels()...)
	c.Model(&model.GaClient{}).AddForeignKey("client_id", "clients(id)", "CASCADE", "CASCADE")
	c.Model(&model.GaSession{}).AddForeignKey("ga_client_id", "ga_clients(id)", "CASCADE", "CASCADE")
	c.Model(&model.GaSession{}).AddForeignKey("ga_referer_id", "ga_referers(id)", "CASCADE", "CASCADE")
	c.Model(&model.GaReferer{}).AddForeignKey("gaa_profile_id", "gaa_profiles(id)", "CASCADE", "CASCADE")
	c.Model(&model.GaSession{}).AddForeignKey("gaa_profile_id", "gaa_profiles(id)", "CASCADE", "CASCADE")
	c.Model(&model.GaaProfile{}).AddForeignKey("gaa_web_property_id", "gaa_web_properties(id)", "CASCADE", "CASCADE")
	c.Model(&model.GaaWebProperty{}).AddForeignKey("gaa_account_id", "gaa_accounts(id)", "CASCADE", "CASCADE")
	c.Model(&model.MCFBasic{}).AddForeignKey("mcf_report_id", "mcf_reports(id)", "CASCADE", "CASCADE")
	c.Exec("ALTER TABLE users_gaa_account ADD CONSTRAINT fk_gaa_account_id FOREIGN KEY (gaa_account_id) REFERENCES gaa_accounts(id)")
	c.Exec("ALTER TABLE users_gaa_account ADD CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES users(id)")
}

func Clear() {
	c := db.GetDB()
	c.Exec("DROP TABLE users_gaa_account;")
	c.DropTableIfExists(db.GetAllModels()...)
}
