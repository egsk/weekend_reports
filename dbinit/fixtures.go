package dbinit

import (
	"vst/db"
	"vst/model"
	"vst/repository"
	"vst/service/analytics/google/integration"
	"vst/service/userhelper"
)

//Fixtures загружает тестовые данные в DB
func Fixtures() {
	c := db.GetDB()

	ur := repository.UserRepository{}

	user := model.User{
		Username: "admin",
		Password: "admin",
		Role:     "ROLE_ADMIN",
	}
	c.FirstOrCreate(userhelper.PrepareUser(&user))
	gai.SaveAccounts()
	u := model.User{
		Username: "sasha_alex",
		Password: "sasha_alex",
		Role:     "ROLE_USER",
	}
	c.Create(userhelper.PrepareUser(&u))
	sa := model.GaaAccount{}
	c.Where("name = 'Sasha | Alex'").Take(&sa)
	user.GaaAccounts = []model.GaaAccount{sa}
	ur.AppendGaaAccounts(&u)
}
