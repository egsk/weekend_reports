# WeekendDocs

Внутренние отчёты

## Документация к внешнему API внутренних отчётов

### Безопасность и управление

* #### Авторизация

 Запрос:
```curl
curl -X POST \
  <URL>/login \
  -H 'Content-Type: application/json' \
  -d '{"username": "<username>", "password":"<password>"}'
```
Ответ:
```
STATUS: 200;
BODY: {"token": "<token>"}
```


* #### Создание нового пользователя

 Запрос:
```curl
curl -X POST \
  <URL>/api/user/add \
  -H 'Authorization: Bearer <token>' \
  -H 'Content-Type: application/json' \
  -d '{
	"username": "<username>",
	"password": "<password>",
	"accoundsID": ["<google_analytics_account_id_1>", "<google_analytics_account_id_2>"]
}'
```
Ответ:
```
STATUS: 201
```

### Работа с Google Analytics

* #### Запрос дерева доступных аккаунтов Google Analytics
 Запрос:
```curl
curl -X GET \
  <URL>/api/analytics/google/accounts \
  -H 'Authorization: Bearer <token>' \
  -H 'Content-Type: application/json' \
}'
```
Ответ:
```
CODE: 200;
BODY: {
    "accounts": [
        {
            "id": "<account_id>",
            "name": "<account_name>",
            "web_properties": [
                {
                    "id": "<web_property_id>",
                    "name": "<web_property_name>",
                    "profiles": [
                        {
                            "id": "<profile_id>",
                            "name": "<profile_name>"
                        }
                    ]
                }
            ]
        }
    ]
}
```

### Работа с сессиями Google Analytics

* #### Сбор сессий представления (profile) Google Analytics
 Запрос:
```curl
curl -X POST \
  <URL>/api/analytics/google/sessions/collect \
  -H 'Authorization: Bearer <token>' \
  -H 'Content-Type: application/json' \
  -d '{
	"profile_id": "<google_analytics_profile_id>",
	"date_start": "<YYYY-MM-DD>",
	"date_end": "<YYYY-MM-DD>"
}'
```
Ответ:
```
    CODE: 200;
    BODY: {"message": "Sessions successfully collected"}
```

* #### Получение списка дат собраных сессий (profile) Google Analytics
 Запрос:
```curl
curl -X GET \
  <URL>/api/analytics/google/sessions/available/<google_analytics_profile_id> \
  -H 'Authorization: Bearer <token>' \
  -H 'Content-Type: application/json' \
}'
```
Ответ:
```
    CODE: 200;
    BODY: {
        "dates": [
            "<iso_date1",
            "<iso_date2>"
        ]
    }
```


### Работа с внутренними отчётами

* #### Создание отчёта
 Запрос:
```curl
curl -X POST \
  <URL>/api/mcf-report/create \
  -H 'Authorization: Bearer <token>' \
  -H 'Content-Type: application/json' \
  -d '{
	"profile_id": "<google_analytics_profile_id>",
	"date_start": "<YYYY-MM-DD>",
	"date_end": "<YYYY-MM-DD>"
}'
```
Ответ:
```
    CODE: 200
```

* #### Обновление отчёта
 Запрос:
```curl
curl -X POST \
  <URL>/api/mcf-report/create \
  -H 'Authorization: Bearer <token>' \
  -H 'Content-Type: application/json' \
  -d '{
	"profile_id": "<google_analytics_profile_id>",
	"report_id": "<report_id>",
	"date_start": "<YYYY-MM-DD>",
	"date_end": "<YYYY-MM-DD>"
}'
```
 Ответ:
```
    CODE: 200
```
* #### Список созданных отчётов
 Запрос:
```curl
curl -X GET \
  <URL>/api/mcf-report/list \
  -H 'Authorization: Bearer <token>' \
  -H 'Content-Type: application/json'
```
 Ответ:
```
CODE: 200
BODY: [
          {
              "id": <report_id>,
              "start_date": "<iso_date>",
              "end_date": "<iso_date>",
              "status": <status>,
              "profile": {
                  "id": "<google_analytics_profile_id>",
                  "name": "<google_analytics_profile_name>"
                  "web_property": {
                      "id": "<google_analytics_web_property_id>",
                      "name": "<google_analytics_web_property_name>",
                      "account": {
                          "id": "<google_analytics_account_id>",
                          "name": "<google_analytics_account_id>",
                      }
                  },
              },
              "start_time": "<iso_time>",
              "end_time": "<iso_time>"
          }
      ]
```

* #### Получение отчёта
 Запрос:
```curl
curl -X GET \
  <URL>/api/mcf-report/<id> \
  -H 'Authorization: Bearer <token>' \
  -H 'Content-Type: application/json'
```
 Ответ:
```
CODE: 200
BODY: {
          "header": [
              {
                  "type": "<type_of_header>",
                  "val": "<displayable_name>",
                  "model": "<agregation_model>",
                  "visible": <initial_visibility>
              }
          ],
          "body": [
            [
                "<value>"
            ]
          ]
```        