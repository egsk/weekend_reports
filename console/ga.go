package console

import (
	ga "google.golang.org/api/analyticsreporting/v4"
	"log"
	"vst/repository"
	"vst/service/analytics/google/integration"
	"vst/service/analytics/google/native"
)

func GA(command string) {
	GaaProfile := repository.GaaProfileRepository{}.Find("181954238")
	vp := gan.ReportParams{
		GaaProfile: GaaProfile,
		DateRanges: []*ga.DateRange{
			{
				StartDate: "2018-12-01",
				EndDate:   "2019-12-31",
			},
		},
	}
	switch command {
	case "save":
		gai.SaveAccounts()
	case "1":
		gan.InternalReport(&vp)
		gai.CollectRawData(&vp)
	case "2":
		gan.InternalReport(&vp)
		gai.CollectRevenueData(&vp)
	case "3":
		gan.InternalReport(&vp)
		gai.FilterSessions(&vp)
	case "basic":
		gan.InternalReport(&vp)
		//mcf.BasicAttribution(&vp)
	case "test":
		gan.InternalReport(&vp)
		gai.DisplayData(&vp, vp.Reports)
	default:
		log.Print("Command does not exist")
	}
}
