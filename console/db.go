package console

import (
	"log"
	"vst/db"
	"vst/dbinit"
)

//DB выполняет консольные команды, связанные с консолью
func DB(command string) {
	db.GetDB().LogMode(true)
	switch command {
	case "drop":
		db.GetDB().DropTableIfExists(db.GetAllModels()...)
		log.Println("ok")
	case "migrate":
		dbinit.Migrate()
		log.Println("ok")
	case "fixtures":
		dbinit.Fixtures()
		log.Println("ok")
	case "restart":
		dbinit.Clear()
		dbinit.Migrate()
		dbinit.Fixtures()
		log.Println("ok")
	default:
		log.Println("Команды " + command + " не существует")
	}
}
