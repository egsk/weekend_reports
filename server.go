package main

import (
	"os"
	"vst/route"

	// "vst/route"
)

func main() {

	if len(os.Args) > 1 && os.Args[1] == "console" {
		modConsole()
	} else {
		e := route.Init()
		e.Logger.Info(e.Start(":1321"))
	}
}
