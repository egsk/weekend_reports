package controller

import (
	"net/http"
	"vst/db"
	"vst/model"
	"vst/service/security"
	"vst/service/userhelper"

	"github.com/labstack/echo"
)

//Login роут для авторизации
//REQUEST:
//curl -X POST \
//http://api.com/login\
//-H 'Content-Type: application/json' \
//-d '{"username": "<username>", "password": "<password>"}'
//RESPONSE:
//{
//    "token": ""
//}
func Login(c echo.Context) (err error) {
	conn := db.GetDB()
	var u, auth model.User
	if err = c.Bind(&u); err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}

	conn.Where("username = ?", u.Username).First(&auth)
	if auth.ID == 0 || !userhelper.CompareHashAndPassword(auth.HashedPassword, u.Password) {
		return c.JSON(http.StatusUnauthorized, echo.Map{"message": "Bad Credentials"})
	}

	t := security.CreateToken(&auth)
	return c.JSON(http.StatusOK, echo.Map{
		"token": t,
	})
}
