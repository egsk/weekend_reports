package controller

import (
	"github.com/labstack/echo"
	ga "google.golang.org/api/analyticsreporting/v4"
	"net/http"
	"time"
	"vst/middleware/user"
	"vst/model"
	"vst/repository"
	"vst/service/analytics/google/integration"
	"vst/service/analytics/google/mcf"
	"vst/service/analytics/google/native"
	"vst/service/security/securityhelper"
)

type request struct {
	ProfileID string `json:"profile_id"`
	DateStart string `json:"date_start"`
	DateEnd   string `json:"date_end"`
	ReportID  uint   `json:"report_id"`
}

//func CreateReport(c echo.Context) error {
//
//	rp, err := getRequestParams(c)
//	if err != nil {
//		return c.JSON(http.StatusBadRequest, err)
//	}
//	err = gan.RefererData(rp)
//	if err != nil {
//		return c.JSON(http.StatusInternalServerError, err)
//	}
//
//	if rp.MCFReport.ID == 0 {
//		err = mcf.CreateReport(rp)
//	} else {
//		rr := repository.MCFReportRepository{}
//		report := rr.Find(rp.MCFReport.ID)
//		if report.ID == 0 {
//			return c.JSON(http.StatusNotFound, echo.Map{"message":
//			"Requested report does not exist"})
//		}
//		err = mcf.UpdateReport(rp)
//	}
//	if err != nil {
//		return c.JSON(http.StatusInternalServerError, err)
//	}
//
//	return c.JSON(http.StatusOK, echo.Map{"message": "Отчёт создан"})
//}

func GetAccounts(c echo.Context) error {
	var accounts *[]model.GaaAccount

	gar := repository.GaaAccountRepository{}
	u := c.(*user.Context).User
	if securityhelper.IsGranted(&u, "ROLE_ADMIN", false) {
		accounts = gar.FindAllWithRelations()
	} else {
		accounts = gar.FindWithRelationsByUser(u)
	}

	return c.JSON(200, echo.Map{
		"accounts": accounts,
	})
}

func GetMFCByParams(c echo.Context) error {
	var mcfRows []model.MCFBasic
	var report *model.MCFReport
	rr := repository.MCFReportRepository{}
	rb := repository.MCFBasicRepository{}

	u := c.(*user.Context).User
	if !securityhelper.IsGranted(&u, "ROLE_ADMIN", false) {
		report = rr.FindIncludeRelations(c.Param("id"))
		userCanAccess := false
		for _, us := range report.GaaProfile.GaaWebProperty.GaaAccount.Users {
			if us.ID == u.ID {
				userCanAccess = true
			}
		}
		if !userCanAccess {
			return c.JSON(http.StatusForbidden, echo.Map{"message": "Access denied"})
		}
	} else {
		report = rr.Find(c.Param("id"))
	}
	if report.ID == 0 {
		return c.JSON(http.StatusNotFound, echo.Map{"message": "Report not found"})
	}
	mcfRows = rb.FindAllByMCFReport(report)
	for i := 0; i < len(mcfRows); i++ {
		mcfRows[i].TotalRevenue = mcfRows[i].Revenue + mcfRows[i].AssociatedRevenue
	}
	r, err := gai.CreateResponseRequest(&mcfRows)
	if err != nil {
		return err
	}
	r.Meta = report

	return c.JSON(200, r)
}

func GetReportsList(c echo.Context) error {

	var reports *[]model.MCFReport

	rr := repository.MCFReportRepository{}
	currentUser := c.(*user.Context).User

	if securityhelper.IsGranted(&currentUser, "ROLE_ADMIN", false) {
		reports = rr.FindAllIncludeRelations()
	} else {
		reports = rr.FindByUser(currentUser)
	}
	return c.JSON(200, reports)
}

func CollectSessions(c echo.Context) error {
	rp, err := getRequestParams(c)
	if err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}
	err = mcf.CollectSessions(rp)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}
	return c.JSON(http.StatusOK, echo.Map{"message": "Sessions successfully collected"})
}

func CreateReport(c echo.Context) error {
	rp, err := getRequestParams(c)
	if err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}
	if rp.MCFReport.ID == 0 {
		err = mcf.CreateReport(rp)
	} else {
		rr := repository.MCFReportRepository{}
		report := rr.Find(rp.MCFReport.ID)
		if report.ID == 0 {
			return c.JSON(http.StatusNotFound, echo.Map{"message":
			"Requested report does not exist"})
		}
		err = mcf.UpdateReport(rp)
	}
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}

	return c.JSON(http.StatusOK, nil)
}

func GetAvailableSessionDates(c echo.Context) error {
	gp := model.GaaProfile{}

	gp.ID = c.Param("id")
	sr := repository.GaSessionRepository{}

	dates := sr.FindAvailableDaysOfProfile(gp)

	return c.JSON(http.StatusOK, echo.Map{"dates": dates})
}

func getRequestParams(c echo.Context) (*gan.ReportParams, error) {
	var err error
	gpr := repository.GaaProfileRepository{}
	reportRepository := repository.MCFReportRepository{}
	r := request{}
	if err = c.Bind(&r); err != nil {
		return nil, err
	}
	gp := gpr.Find(r.ProfileID)
	report := reportRepository.Find(r.ReportID)
	report.StartDate, err = time.Parse("2006-01-02", r.DateStart)
	if err != nil {
		return nil, err
	}
	report.EndDate, err = time.Parse("2006-01-02", r.DateEnd)
	if err != nil {
		return nil, err
	}

	rp := gan.ReportParams{
		GaaProfile: gp,
		DateRanges: []*ga.DateRange{
			{
				StartDate: r.DateStart,
				EndDate:   r.DateEnd,
			},
		},
		MCFReport: report,
	}

	return &rp, nil
}
