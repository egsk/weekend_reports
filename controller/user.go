package controller

import (
	"net/http"
	"vst/db"
	"vst/middleware/user"
	"vst/model"
	"vst/repository"
	"vst/service/userhelper"

	"github.com/labstack/echo"
)

func GetUser(c echo.Context) error {
	return c.JSON(http.StatusOK, c.(*user.Context).User)
}

func CreateUser(c echo.Context) error {
	var u model.User
	if err := c.Bind(&u); err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}
	u.Role = "ROLE_USER"
	u = *userhelper.PrepareUser(&u)
	errs := db.GetDB().Create(&u).GetErrors()
	if len(errs) > 0 {
		return c.JSON(http.StatusBadRequest, errs)
	}
	ur := repository.UserRepository{}
	ur.AppendGaaAccounts(&u)
	return c.JSON(http.StatusCreated, u.GaaAccountsId)
}
