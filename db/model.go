package db

import (
	"vst/model"
)

//GetAllModels возвращает список всех моделей
func GetAllModels() []interface{} {
	return []interface{}{
		&model.MCFBasic{},
		&model.MCFReport{},
		&model.GaSession{},
		&model.GaReferer{},
		&model.GaClient{},
		&model.User{},
		&model.GaaProfile{},
		&model.GaaWebProperty{},
		&model.GaaAccount{},
		&model.Client{},
	}
}
