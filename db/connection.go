package db

import (
	"fmt"
	"vst/config"

	//Обеспечение работоспособности драйвера postgres
	_ "github.com/jinzhu/gorm/dialects/postgres"

	"github.com/jinzhu/gorm"
)

var conn *gorm.DB
var isInitilized bool

//GetDB возвращает подключение к базе данных
func GetDB() *gorm.DB {
	if !isInitilized {
		initilize()
	}
	return conn
}

func initilize() {
	var err error

	c := config.Get().Db
	conn, err = gorm.Open(
		"postgres",
		fmt.Sprintf("host=%s port=%d user=%s dbname=%s password=%s sslmode=%s",
			c.Host, c.Port, c.User, c.Name, c.Password, c.SSLMode))
	if err != nil {
		panic(err)
	}
	isInitilized = true
}
