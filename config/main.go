package config

var g General
var initilized bool

//General - глобальный конфиг, содержащий в себе остальные
type General struct {
	Db       Db
	Security Security
	Routing  []Route
}

//Get - возвращает ссылку на глобальный конфиг приложения
func Get() *General {

	if !initilized {
		initilize()
	}

	return &g
}

func initilize() {
	g = General{
		Db:       initDb(),
		Security: initSecurity(),
		Routing:  initRouting(),
	}
	initilized = true
}
