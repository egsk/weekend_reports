package config

import (
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

func initRouting() []Route {
	var routes []Route
	data, err := ioutil.ReadFile("_config/yaml/routing.yaml")
	if err != nil {
		panic(err)
	}
	err = yaml.Unmarshal(data, &routes)
	if err != nil {
		panic(err)
	}

	return routes
}

//Route тип для хранения мета-информации о роуте
type Route struct {
	Path        string
	Requirement string
	Exact       bool
}
