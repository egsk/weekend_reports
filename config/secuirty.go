package config

import (
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

//Security секьюрити-конфиг
type Security struct {
	Secret string
	Roles  []string
}

func initSecurity() Security {
	security := Security{}
	data, err := ioutil.ReadFile("_config/yaml/security.yaml")
	if err != nil {
		panic(err)
	}
	err = yaml.Unmarshal(data, &security)
	if err != nil {
		panic(err)
	}

	return security
}
