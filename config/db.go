package config

import (
	"io/ioutil"
	"gopkg.in/yaml.v2"
)

func initDb() Db {
	db := Db{}
	data, err := ioutil.ReadFile("_config/yaml/db.yaml")
	if err != nil {
		panic(err)
	}
	err = yaml.Unmarshal(data, &db)
	if err != nil {
		panic(err)
	}

	return db
}

//Db конфиг соединения с базой данных
type Db struct {
	Driver   string
	Host     string
	Port     uint16
	Name     string
	User     string
	Password string
	SSLMode  string
}
