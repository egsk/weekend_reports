package main

import (
	"log"
	"os"
	"vst/console"
)

func modConsole() {
	args := os.Args[2:]
	if len(args) > 0 {
		switch args[0] {
		case "db":
			if len(args) == 1 {
				log.Fatal("Отсутстует аргумент для функции DB")
				return
			}
			console.DB(args[1])
		case "ga":
			console.GA(args[1])
		default:
			log.Fatal("Незивестная команда")
		}
	}
}
