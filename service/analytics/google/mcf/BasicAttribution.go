package mcf

import (
	"time"
	"vst/db"
	"vst/model"
	"vst/repository"
	"vst/service/analytics/google/integration"
	"vst/service/analytics/google/native"
	"vst/service/helper/th"
)

//func BasicAttribution(params *gan.ReportParams) error {
//	var e error
//
//	startDate, err := th.StrToTime(params.DateRanges[0].StartDate)
//	if err != nil {
//		return err
//	}
//	endDate, err := th.StrToTime(params.DateRanges[0].EndDate)
//	if err != nil {
//		return err
//	}
//	reportStatus := model.MCFReport{
//		StartTime:    time.Now(),
//		StartDate:    startDate,
//		EndDate:      endDate,
//		GaaProfileID: params.GaaProfile.ID,
//		Status:       0,
//	}
//	db.GetDB().Where("gaa_profile_id = ? AND start_date::date = ? AND end_date::date = ?",
//		params.GaaProfile.ID, params.DateRanges[0].StartDate, params.DateRanges[0].EndDate).
//		Assign(&reportStatus).FirstOrCreate(&reportStatus)
//
//	e = gan.InternalReport(params)
//	if e != nil {
//		noticeError(&reportStatus)
//		return e
//	}
//	e = CollectSessions(params)
//	if e != nil {
//		noticeError(&reportStatus)
//		return e
//	}
//	e = ComputeByExistingSessions(params)
//	if e != nil {
//		noticeError(&reportStatus)
//		return e
//	}
//	reportStatus.Status = 1
//	reportStatus.EndTime = time.Now()
//	db.GetDB().Save(&reportStatus)
//
//	return e
//}

func CollectSessions(params *gan.ReportParams) error {
	var e error

	e = gan.SessionData(params)
	if e != nil {
		return e
	}
	e = gai.CollectRawData(params)
	if e != nil {
		return e
	}
	e = gai.CollectRevenueData(params)
	if e != nil {
		return e
	}
	e = gai.FilterSessions(params)
	if e != nil {
		return e
	}
	return e
}

func CreateReport(params *gan.ReportParams) error {

	startDate, err := th.StrToTime(params.DateRanges[0].StartDate)
	if err != nil {
		return err
	}
	endDate, err := th.StrToTime(params.DateRanges[0].EndDate)
	if err != nil {
		return err
	}
	reportStatus := model.MCFReport{
		StartTime:    time.Now(),
		StartDate:    startDate,
		EndDate:      endDate,
		GaaProfileID: params.GaaProfile.ID,
		Status:       0,
	}
	db.GetDB().Create(&reportStatus)
	params.MCFReport = &reportStatus

	err = ComputeByExistingSessions(params)
	if err != nil {
		noticeError(&reportStatus)
		return err
	}
	reportStatus.Status = 1
	reportStatus.EndTime = time.Now()
	db.GetDB().Save(&reportStatus)

	return nil
}

func UpdateReport(params *gan.ReportParams) error {

	r := params.MCFReport

	db.GetDB().Where("mcf_report_id = ?", r.ID).Delete(model.MCFBasic{})

	r.Status = 0
	r.StartTime = time.Now()

	db.GetDB().Save(r)

	err := ComputeByExistingSessions(params)

	if err != nil {
		noticeError(params.MCFReport)
		return err
	}

	r.Status = 1
	r.EndTime = time.Now()
	db.GetDB().Save(r)

	return nil
}

func ComputeByExistingSessions(params *gan.ReportParams) error {
	cr := repository.GaRefererRepository{}
	err := gan.RefererData(params)
	if err != nil {
		return err
	}
	referers := cr.FindByGaaProfile(params.GaaProfile)
	computeReferers(params, &referers)
	err = gai.CollectRefererData(params, &referers)
	if err != nil {
		return err
	}
	return nil
}

func noticeError(m *model.MCFReport) {
	m.Status = -1
	m.EndTime = time.Now()
	db.GetDB().Save(m)
}

func computeReferers(params *gan.ReportParams, referers *[]model.GaReferer) {
	cr := repository.GaClientRepository{}
	clients := cr.FindWithRevenue(params)

	for _, c := range *clients {
		sessions := &c.GaSessions
		var index int
		for i := len(*sessions) - 1; i >= 0; i-- {
			if c.GaSessions[i].Revenue > 0 {
				index = i
				break
			}
		}
		st := (*sessions)[0 : index+1]
		sessions = &st
		var summaryWeight int
		var summaryRevenue float64
		for sIndex := range *sessions {
			summaryWeight++
			summaryRevenue += (*sessions)[sIndex].Revenue
			for _, r := range *referers {
				if r.ID == (*sessions)[sIndex].GaReferer.ID {
					r.MCFWeight++
					break
				}
			}
		}

		for sIndex := range *sessions {
			for rIndex := range *referers {
				if (*referers)[rIndex].ID == (*sessions)[sIndex].GaReferer.ID {
					(*referers)[rIndex].MCFRevenue += summaryRevenue / float64(summaryWeight)
					if len(*sessions) >= 2 {
						(*referers)[rIndex].MCFReducedRevenue += summaryRevenue / float64(summaryWeight)
					}
					break
				}
			}
		}

	}
}
