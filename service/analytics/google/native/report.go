package gan

import (
	ga "google.golang.org/api/analyticsreporting/v4"
	"vst/model"
)

type ReportParams struct {
	GaaProfile     *model.GaaProfile
	DateRanges     []*ga.DateRange
	Reports        ReportSet
	MCFReport      *model.MCFReport
	reportRequests []*ga.ReportRequest
}

type ReportSet [][]*ga.Report

//InternalReport запрашивает все данные, необходимые для построения внутреннего отчёта, создавая запросы:
//1. Сбор клиентов, сессий и рефереров для их сохранения в базе данныхх
//2. Сбор сессий и доходов: заполнение уже сформированных сессий доходом
//3. Сбор дополнительных показателей рефереров
func InternalReport(params *ReportParams) error {
	params.reportRequests = []*ga.ReportRequest{
		{
			ViewId:     params.GaaProfile.ID,
			DateRanges: params.DateRanges,
			Metrics: createMetrics([]string{
				"ga:users",
			}),
			Dimensions: createDimensions([]string{
				"ga:dimension1",
				"ga:dateHourMinute",
				"ga:source",
				"ga:medium",
				"ga:keyword",
				"ga:adContent",
			}),
			PageSize: 10000,
			OrderBys: []*ga.OrderBy{{
				FieldName: "ga:dimension1",
			}, {
				FieldName: "ga:dateHourMinute",
			}},
		},
		{
			ViewId:     params.GaaProfile.ID,
			DateRanges: params.DateRanges,
			PageSize: 10000,
			Metrics: createMetrics([]string{
				"ga:transactionRevenue",
			}),
			Dimensions: createDimensions([]string{
				"ga:dimension1",
				"ga:dateHourMinute",
			}),
		},
		{
			ViewId:     params.GaaProfile.ID,
			DateRanges: params.DateRanges,
			Metrics: createMetrics([]string{
				"ga:transactionRevenue",
				"ga:adCost",
				"ga:adClicks",
				"ga:CPC",
				"ga:impressions",
				"ga:bounceRate",
				"ga:avgSessionDuration",
				"ga:revenuePerTransaction",
				"ga:transactions",
				"ga:sessions",
			}),
			PageSize: 10000,
			Dimensions: createDimensions([]string{
				"ga:source",
				"ga:medium",
				"ga:keyword",
				"ga:adContent",
			}),
		},
	}
	return getAllReports(params)
}

func SessionData(params *ReportParams) error {
	params.reportRequests = []*ga.ReportRequest{
		{
			ViewId:     params.GaaProfile.ID,
			DateRanges: params.DateRanges,
			Metrics: createMetrics([]string{
				"ga:users",
			}),
			Dimensions: createDimensions([]string{
				"ga:dimension1",
				"ga:dateHourMinute",
				"ga:source",
				"ga:medium",
				"ga:keyword",
				"ga:adContent",
			}),
			OrderBys: []*ga.OrderBy{{
				FieldName: "ga:dimension1",
			}, {
				FieldName: "ga:dateHourMinute",
			}},
		},
		{
			ViewId:     params.GaaProfile.ID,
			DateRanges: params.DateRanges,
			Metrics: createMetrics([]string{
				"ga:transactionRevenue",
			}),
			Dimensions: createDimensions([]string{
				"ga:dimension1",
				"ga:dateHourMinute",
			}),
		},
	}
	return getAllReports(params)
}

func RefererData(params *ReportParams) error {
	params.reportRequests = []*ga.ReportRequest{
		{
			ViewId:     params.GaaProfile.ID,
			DateRanges: params.DateRanges,
			Metrics: createMetrics([]string{
				"ga:transactionRevenue",
				"ga:adCost",
				"ga:adClicks",
				"ga:CPC",
				"ga:impressions",
				"ga:bounceRate",
				"ga:avgSessionDuration",
				"ga:revenuePerTransaction",
				"ga:transactions",
				"ga:sessions",
			}),
			Dimensions: createDimensions([]string{
				"ga:source",
				"ga:medium",
				"ga:keyword",
				"ga:adContent",
			}),
		},
	}
	return getAllReports(params)
}

//Собирает данные с учётом пагинации
func getAllReports(params *ReportParams) error {
	reports := make(ReportSet, len(params.reportRequests))
	svc, _ := getServiceClient()

	result, err := svc.Reports.BatchGet(&ga.GetReportsRequest{
		ReportRequests: params.reportRequests,
	}).Do()

	if err != nil {
		return err
	}

	for i, report := range result.Reports {
		reports[i] = append(reports[i], report)
		for report.NextPageToken != "" {
			params.reportRequests[i].PageToken = report.NextPageToken
			req := &ga.GetReportsRequest{ReportRequests: params.reportRequests}
			nextReport, err := svc.Reports.BatchGet(req).Do()
			if err != nil {
				return err
			}
			report = nextReport.Reports[0]
			reports[i] = append(reports[i], report)
		}
	}
	params.Reports = reports
	return nil
}
