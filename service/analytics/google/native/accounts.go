package gan

import (
	"google.golang.org/api/analytics/v3"
)

func GetAccounts() *analytics.AccountSummaries  {
	_, management := getServiceClient()
	accounts, _ := management.Management.AccountSummaries.List().Do()

	return accounts
}
