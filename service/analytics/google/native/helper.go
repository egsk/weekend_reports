package gan

import (
	ga "google.golang.org/api/analyticsreporting/v4"
)
func createMetrics(metrics []string) []*ga.Metric {
	var r []*ga.Metric
	for _, metric := range metrics {
		r = append(r, &ga.Metric{
			Expression: metric,
		})
	}
	return r
}

func createDimensions(dimensions []string) []*ga.Dimension {
	var r []*ga.Dimension
	for _, dimension := range dimensions {
		r = append(r, &ga.Dimension{
			Name: dimension,
		})
	}
	return r
}

