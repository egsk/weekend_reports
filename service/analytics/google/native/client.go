package gan

import (
	"io/ioutil"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"

	gam "google.golang.org/api/analytics/v3"
	ga "google.golang.org/api/analyticsreporting/v4"
)

var service *ga.Service
var management *gam.Service
var initilized bool
var credentials = "_config/json/google_analytics_credentials.json"

func getServiceClient() (*ga.Service, *gam.Service) {

	if initilized {
		return service, nil
	}
	data, err := ioutil.ReadFile(credentials)
	if err != nil {
		panic(err)
	}
	conf, err := google.JWTConfigFromJSON(data, ga.AnalyticsReadonlyScope)
	if err != nil {
		panic(err)
	}
	netClient := conf.Client(oauth2.NoContext)
	service, err = ga.New(netClient)
	if err != nil {
		panic(err)
	}
	management, err = gam.New(netClient)
	if err != nil {
		panic(err)
	}
	initilized = true
	return service, management
}

