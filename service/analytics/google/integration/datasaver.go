package gai

import (
	"fmt"
	"io/ioutil"
	"vst/service/analytics/google/native"
)

type report func(vp *gan.ReportParams) gan.ReportSet

func DisplayData(vp *gan.ReportParams, data gan.ReportSet) {
	reportText := ""
	divider := "\n\r==============================\n\r"
	for reportIndex, reportPages := range data {
		for pageIndex, report := range reportPages {
			reportText +=
				fmt.Sprintf(`%sNew Report number %d.%d%s`, divider, reportIndex+1, pageIndex+1, divider)
			for j, row := range report.Data.Rows {
				reportText += fmt.Sprintf("%sRow %d\n\r", divider, j+1)
				for _, d := range row.Dimensions {
					reportText += fmt.Sprintf("%s ", d)
				}
				for _, m := range row.Metrics {
					for _, v := range m.Values {
						reportText += fmt.Sprintf("%s ", v)
					}
				}
			}
		}
	}
	err := ioutil.WriteFile("report.txt", []byte(reportText), 777)
	if err != nil {
		panic(err)
	}
	fmt.Println("done")
}