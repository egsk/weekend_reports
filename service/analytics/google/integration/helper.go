package gai

import (
	"strconv"
	"time"
)

func formatGoogleTime(date string) time.Time {
	year, err := strconv.Atoi(date[0:4])
	if err != nil {
		panic(err)
	}
	monthNum, err := strconv.Atoi(date[4:6])
	if err != nil {
		panic(err)
	}
	month := time.Month(monthNum)
	day, err := strconv.Atoi(date[6:8])
	if err != nil {
		panic(err)
	}
	hour, err := strconv.Atoi(date[8:10])
	if err != nil {
		panic(err)
	}
	min, err := strconv.Atoi(date[10:12])
	if err != nil {
		panic(err)
	}
	return time.Date(year, month, day, hour, min, 0, 0, time.UTC)
}
