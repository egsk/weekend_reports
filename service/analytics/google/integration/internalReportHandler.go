package gai

import (
	"strconv"
	"vst/db"
	"vst/model"
	"vst/repository"
	"vst/service/analytics/google/native"
)

func CollectRawData(params *gan.ReportParams) error {
	reports := params.Reports[0]
	if len(reports) == 0 || len(reports[0].Data.Rows) == 0 {
		return nil
	}
	conn := db.GetDB()
	conn.Debug().
		Where("gaa_profile_id = ? and (time::date >= ? OR time::date <= ?)",
			params.GaaProfile.ID,
			params.DateRanges[0].StartDate,
			params.DateRanges[0].EndDate).
		Delete([]*model.GaSession{})
	tx := conn.Begin()
	refererRepository := repository.GaRefererRepository{}
	gaClientRepository := repository.GaClientRepository{}
	gaaProfile := params.GaaProfile
	cid := reports[0].Data.Rows[0].Dimensions[0]
	prevGaClient := gaClientRepository.Find(cid)
	refs := refererRepository.FindByGaaProfile(gaaProfile)
	prevTime := ""
	for _, report := range reports {
		for _, row := range report.Data.Rows {
			var currentGaClient *model.GaClient
			cid := row.Dimensions[0]
			if prevGaClient.ID != cid {
				currentGaClient = gaClientRepository.Find(cid)
			} else if prevTime == row.Dimensions[1] {
				continue
			} else {
				currentGaClient = prevGaClient
			}
			sid := cid + "_" + row.Dimensions[1]
			if currentGaClient.ID == "" {
				currentGaClient.ID = cid
				tx.Create(currentGaClient)
			}
			currentRef := getReferer(&refs, row.Dimensions, params)
			session := &model.GaSession{
				ID:           sid,
				Time:         formatGoogleTime(row.Dimensions[1]),
				GaaProfileID: &gaaProfile.ID,
				GaRefererID:  &currentRef.ID,
				GaClientID:   &currentGaClient.ID,
			}
			tx.Create(session)
			prevGaClient = currentGaClient
			prevTime = row.Dimensions[1]
		}
	}
	tx.Commit()
	return tx.Error
}

func CollectRevenueData(params *gan.ReportParams) error {
	reports := params.Reports[1]
	if len(reports) == 0 || len(reports[0].Data.Rows) == 0 {
		return nil
	}
	sessionRepository := repository.GaSessionRepository{}
	tx := db.GetDB().Begin()
	for _, report := range reports {
		for _, row := range report.Data.Rows {
			sid := row.Dimensions[0] + "_" + row.Dimensions[1]
			s := sessionRepository.Find(sid)
			val, _ := strconv.ParseFloat(row.Metrics[0].Values[0], 64)
			s.Revenue = val
			tx.Save(s)
		}
	}
	tx.Commit()
	return tx.Error
}

func FilterSessions(params *gan.ReportParams) error {
	cr := repository.GaClientRepository{}
	clients := cr.FindWithRevenue(params)
	tx := db.GetDB().Begin()
	for _, c := range *clients {
		for i := 1; i < len(c.GaSessions); i++ {
			currentS := &c.GaSessions[i]
			prevS := &c.GaSessions[i-1]
			if currentS.Time.Unix()-prevS.Time.Unix() < 5400 && prevS.Revenue == 0 {
				tx.Delete(prevS)
			}
		}
	}
	tx.Commit()

	return tx.Error
}

func CollectRefererData(params *gan.ReportParams, referers *[]model.GaReferer) error {
	reports := params.Reports[len(params.Reports)-1]
	if len(reports) == 0 || len(reports[0].Data.Rows) == 0 {
		return nil
	}
	var mcf []*model.MCFBasic
	db.GetDB().Where("gaa_Profile_id = ? and (start_date::date = ? OR (start_date >= ? AND end_date <= ?))",
		params.GaaProfile.ID,
		params.DateRanges[0].StartDate,
		params.DateRanges[0].StartDate,
		params.DateRanges[0].EndDate).Delete(model.MCFBasic{})
	for _, r := range *referers {
		mcfRow := model.MCFBasic{
			Source:                   r.Source,
			Medium:                   r.Medium,
			Keyword:                  r.Keyword,
			AdContent:                r.AdContent,
			AssociatedRevenue:        r.MCFRevenue,
			AssociatedReducedRevenue: r.MCFReducedRevenue,
			Transactions:             r.Transactions,
			MCFReportID:              params.MCFReport.ID,
		}
		mcf = append(mcf, &mcfRow)
	}
	tx := db.GetDB().Begin()
	for _, rep := range reports {
		for _, row := range rep.Data.Rows {
			for i := range mcf {
				source := truncateRef(row.Dimensions[0])
				medium := truncateRef(row.Dimensions[1])
				keyword := truncateRef(row.Dimensions[2])
				adContent := truncateRef(row.Dimensions[3])
				if source+medium+keyword+adContent ==
					mcf[i].Source+mcf[i].Medium+mcf[i].Keyword+mcf[i].AdContent {
					err := MapRow(mcf[i], rep.ColumnHeader, row)
					if err != nil {
						return err
					}
					tx.Create(mcf[i])
				}
			}
		}
	}
	tx.Commit()

	return tx.Error
}

func getReferer(refs *[]model.GaReferer, dimensions []string, params *gan.ReportParams) *model.GaReferer {

	source := truncateRef(dimensions[2])
	medium := truncateRef(dimensions[3])
	keyword := truncateRef(dimensions[4])
	adContent := truncateRef(dimensions[5])

	looking := source + "/" + medium + "/" + keyword + "/" + adContent
	for _, ref := range *refs {
		refStr := ref.Source + "/" + ref.Medium + "/" + ref.Keyword + "/" + ref.AdContent
		if looking == refStr {
			return &ref
		}
	}
	ref := &model.GaReferer{
		Source:       source,
		Medium:       medium,
		Keyword:      keyword,
		AdContent:    adContent,
		GaaProfileID: &params.GaaProfile.ID,
	}
	db.GetDB().Create(ref)
	*refs = append(*refs, *ref)
	return ref
}

func truncateRef(ref string) string {
	l := 1000
	if len(ref) < l+1 {
		return ref
	}
	return ref[0:l]
}
