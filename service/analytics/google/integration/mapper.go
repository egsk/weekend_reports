package gai

import (
	ga "google.golang.org/api/analyticsreporting/v4"
	"reflect"
	"strconv"
	"strings"
	"vst/model"
)

func MapRow(entity interface{}, columnHeader *ga.ColumnHeader, row *ga.ReportRow) error {
	var err error
	val := reflect.ValueOf(entity).Elem()
	ref := reflect.TypeOf(entity).Elem()
	for i := range row.Dimensions {
		for j := 0; j < val.NumField(); j++ {
			field := ref.Field(j)
			meta := getMap(field, "ga")
			if meta["name"] == columnHeader.Dimensions[i] {
				val.Field(j).SetString(row.Dimensions[i])
			}
		}
	}
	metricHdrs := columnHeader.MetricHeader.MetricHeaderEntries
	for i := range metricHdrs {
		for j := 0; j < ref.NumField(); j++ {
			field := ref.Field(j)
			meta := getMap(field, "ga")
			if meta["name"] == metricHdrs[i].Name {
				switch ref.Field(j).Type.Kind() {
				case reflect.Float64:
					var v float64
					v, err = strconv.ParseFloat(row.Metrics[0].Values[i], 64)
					val.Field(j).SetFloat(v)
				case reflect.Int:
					var v int64
					v, err = strconv.ParseInt(row.Metrics[0].Values[i], 10, 64)
					val.Field(j).SetInt(v)
				case reflect.String:
					val.Field(j).SetString(row.Metrics[0].Values[i])
				case reflect.Uint32:
					var v uint64
					v, err = strconv.ParseUint(row.Metrics[0].Values[i], 10, 64)
					val.Field(j).SetUint(v)
				}
			}
		}
	}

	return err
}

type McfHeader struct {
	Type    string `mcf:"type"json:"type"`
	Value   string `mcf:"val"json:"val"`
	Model   string `mcf:"model"json:"model"`
	Visible bool   `mcf:"visible"json:"visible"`
}

type McfResponse struct {
	Headers []McfHeader     `json:"header"`
	Data    [][]interface{} `json:"body"`
	Meta    *model.MCFReport `json:"meta"`
}

func CreateResponseRequest(mcf *[]model.MCFBasic) (*McfResponse, error) {
	var err error
	var mcfResponse McfResponse
	emptyRow := model.MCFBasic{}
	ref := reflect.TypeOf(emptyRow)
	headers := make([]McfHeader, 0)
	var indexes []int
	for i := 0; i < ref.NumField(); i++ {
		field := ref.Field(i)
		m := getMap(field, "mcf")
		if m["value"] == "" {
			continue
		}
		indexes = append(indexes, i)
		var visible bool
		if m["visible"] != "" {
			visible = true
		} else {
			visible = false
		}
		header := McfHeader{
			Type:    m["type"],
			Model:   m["model"],
			Value:   m["value"],
			Visible: visible,
		}
		headers = append(headers, header)
	}
	data := make([][]interface{}, len(*mcf))
	for i := 0; i < len(*mcf); i++ {
		val := reflect.ValueOf((*mcf)[i])
		for _, j := range indexes {
			switch val.Field(j).Kind() {
			case reflect.Float64:
				data[i] = append(data[i], val.Field(j).Float())
			case reflect.Uint32:
				data[i] = append(data[i], val.Field(j).Uint())
			case reflect.String:
				data[i] = append(data[i], val.Field(j).String())
			}
		}
	}
	mcfResponse.Headers = headers
	mcfResponse.Data = data
	return &mcfResponse, err
}

func getTagFieldValue(s reflect.StructField, mapType, fieldName string) string {
	tag := s.Tag.Get(mapType)
	if len(tag) == 0 {
		return ""
	}
	for _, pair := range strings.Split(tag, ";") {
		val := strings.Split(pair, "=")
		if val[0] == fieldName {
			return val[1]
		}
	}
	return ""
}

func getMap(s reflect.StructField, mapType string) map[string]string {
	meta := make(map[string]string)
	tag := s.Tag.Get(mapType)
	if len(tag) == 0 {
		return nil
	}
	pairs := strings.Split(tag, ";")
	for _, pair := range pairs {
		val := strings.Split(pair, "=")
		meta[val[0]] = val[1]
	}
	return meta
}
