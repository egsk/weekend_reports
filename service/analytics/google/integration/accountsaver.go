package gai

import (
	"vst/db"
	"vst/model"
	"vst/service/analytics/google/native"
)

func SaveAccounts() {
	accounts := gan.GetAccounts()
	conn := db.GetDB()
	for i := range accounts.Items {
		a := accounts.Items[i]
		acc := model.GaaAccount{
			ID: a.Id,
			Name: a.Name,
		}
		conn.Where(&acc).Assign(&acc).FirstOrCreate(&acc)
		for j := range a.WebProperties {
			wp := a.WebProperties[j]
			webp := model.GaaWebProperty{
				ID: wp.Id,
				Name: wp.Name,
				GaaAccountID: acc.ID,
			}
			conn.Where(&webp).Assign(&webp).FirstOrCreate(&webp)
			for k := range wp.Profiles {
				p := wp.Profiles[k]
				pr := model.GaaProfile{
					ID: p.Id,
					Name: p.Name,
					GaaWebPropertyID: webp.ID,
				}
				conn.Where(&pr).Assign(&pr).FirstOrCreate(&pr)
			}
		}
	}
}
