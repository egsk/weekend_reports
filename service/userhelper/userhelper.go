package userhelper

import (
	"vst/model"

	"golang.org/x/crypto/bcrypt"
)

//PrepareUser хэширует пароль
func PrepareUser(user *model.User) *model.User {
	hash, err := bcrypt.GenerateFromPassword([]byte(user.Password), 14)

	if err != nil {
		panic(err)
	}
	user.HashedPassword = string(hash)

	return user
}

//CompareHashAndPassword проверяет валидность пароля
func CompareHashAndPassword(hash, password string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
