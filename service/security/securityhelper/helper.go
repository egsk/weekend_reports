package securityhelper

import (
	"vst/config"
	"vst/model"
)

//IsGranted проверяет, есть ли у текущего пользователя необходимая роль
func IsGranted(u *model.User, role string, exact bool) bool {
	roles := config.Get().Security.Roles

	var currentRoleIndex, requiredRoleIndex int
	for i, val := range roles {
		if val == role {
			requiredRoleIndex = i
		}
		if val == u.Role {
			currentRoleIndex = i
		}
	}
	if exact {
		return currentRoleIndex == requiredRoleIndex
	}
	return currentRoleIndex >= requiredRoleIndex

}
