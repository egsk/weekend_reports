package security

import (
	"time"
	"vst/config"
	"vst/model"

	"github.com/dgrijalva/jwt-go"
)

//JwtCustomClaim - схема приложения для JSON Web Token
type JwtCustomClaim struct {
	Username string `json:"username"`
	Role     string `json:"role"`
	jwt.StandardClaims
}

//CreateToken создаёт токен для пользователя
func CreateToken(u *model.User) string {

	config := config.Get().Security

	claims := &JwtCustomClaim{
		u.Username,
		u.Role,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 72).Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	t, err := token.SignedString([]byte(config.Secret))

	if err != nil {
		panic(err)
	}

	return t
}
