package security

import (
	"net/http"
	"regexp"
	"vst/config"
	"vst/middleware/user"
	"vst/service/security/securityhelper"

	"github.com/labstack/echo"
)

//Middleware миддлвер для контроля допуска к роутам
func Middleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		routes := config.Get().Routing
		currentPath := c.Request().URL.Path
		for _, route := range routes {
			match, err := regexp.MatchString(route.Path, currentPath)
			if err != nil {
				panic(err)
			}
			if match && !securityhelper.IsGranted(&c.(*user.Context).User, route.Requirement, route.Exact) {
				return c.JSON(http.StatusUnauthorized, echo.Map{"message": "Access denied"})
			}
		}
		return next(c)
	}
}
