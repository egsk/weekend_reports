package user

import (
	"vst/db"
	"vst/model"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
)

//Context расширяет стандартный контекст
type Context struct {
	echo.Context
	User model.User
}

//Middleware добавляет текущего пользователя в контекст
func Middleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		data := c.Get("user").(*jwt.Token)
		claims := data.Claims.(jwt.MapClaims)
		user := model.User{}
		conn := db.GetDB()
		conn.Where("username = ?", claims["username"]).Take(&user)
		if user.Role == "" {
			user.Role = "ROLE_ANON"
		}
		uc := &Context{c, user}
		return next(uc)
	}
}
 