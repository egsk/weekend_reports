package model

//User - модель используемая для авторизации
type User struct {
	ID             uint64       `json:"id,omitempty"`
	Username       string       `json:"username" gorm:"type:varchar(100);unique_index"`
	Password       string       `json:"password" gorm:"-"`
	HashedPassword string       `gorm:"column:password"`
	Role           string       `json:"role" gorm:"type:varchar(100)"`
	GaaAccounts    []GaaAccount `gorm:"many2many:users_gaa_account"json:"accounts"`
	GaaAccountsId  []string     `gorm:"-"json:"accountsID"`
}
