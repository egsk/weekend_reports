package model

//Client - модель, определяющая уникального клиента в таблице
type Client struct {
	ID uint `gorm:"primary_key"`
	GaClients []GaClient
}
