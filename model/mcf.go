package model

import (
	"time"
)

type MCFBasic struct {
	ID uint32 `json:"id" gorm:"primary_key;"`
	/**
	Dimensions block
	 */
	Source    string `mcf:"value=source;type=string;visible=true"ga:"name=ga:source"json:"source" gorm:"type:varchar(1000)"`     //Источник
	Medium    string `mcf:"value=medium;type=string;visible=true"ga:"name=ga:medium"json:"medium" gorm:"type:varchar(1000)"`     //Канал
	Keyword   string `mcf:"value=keyword;type=string;visible=true"ga:"name=ga:keyword"json:"keyword"  gorm:"type:varchar(1000)"` //Ключевое слово
	AdContent string `mcf:"value=adContent;type=string;visible=true"json:"ad_content" gorm:"type:varchar(1000)"`                 //Содержание объявления
	/**
	Metrics block
	 */
	Revenue                  float64 `mcf:"value=revenue;type=money;visible=true;model=sum"ga:"name=ga:transactionRevenue"`
	Transactions             uint32  `mcf:"value=transactions;type=number;visible=true;model=sum"ga:"name=ga:transactions"`            //Количество транзакций
	Sessions                 uint32  `mcf:"value=sessions;type=number;visible=true;model=sum"ga:"name=ga:sessions"`                    //Количество сессий
	AdCost                   float64 `mcf:"value=adCost;type=money;visible=true;model=sum"ga:"name=ga:adCost"`                         //Расход на рекламу
	AdClicks                 float64 `mcf:"value=adClicks;type=number;model=sum"ga:"name=ga:adClicks"`                                 //Кол-во кликов
	BounceRate               float64 `mcf:"value=bounceRate;type=percent;model=avg"ga:"name=ga:bounceRate"`                            //Процент отказов
	AvgRevenuePerTransaction float64 `mcf:"value=revenuePerTransaction;type=money;model=avgNotNull"ga:"name=ga:revenuePerTransaction"` //Средний чек
	AssociatedRevenue        float64 `mcf:"value=associatedRevenue;type=money;model=sum"`
	AssociatedReducedRevenue float64 `mcf:"value=associatedReducedRevenue;type=money;model=sum"`
	TotalRevenue             float64 `mcf:"value=totalRevenue;type=money;model=sum"gorm:"-"`
	CPC                      float64 `mcf:"value=CPC;type=money;model=avg"ga:"name=ga:CPC"`
	AvgSessionsDuration      float64 `mcf:"value=avgSessionDuration;type=time;model=avg"ga:"name=ga:avgSessionDuration"`

	MCFReport    *MCFReport
	MCFReportID  uint
}

type MCFReport struct {
	ID           uint       `json:"id"`
	StartDate    time.Time  `json:"start_date"`
	EndDate      time.Time  `json:"end_date"`
	Status       int8       `json:"status"`
	Type         string     `json:"type"gorm:"type:varchar(60)"`
	GaaProfileID string     `gorm:"type:varchar(60)"`
	GaaProfile   GaaProfile `json:"profile"`
	StartTime    time.Time  `json:"start_time"`
	EndTime      time.Time  `json:"end_time"`
}
