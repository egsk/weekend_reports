package model

type GaaProfile struct {
	ID               string         `json:"id" gorm:"type:varchar(50)"`
	GaSessions       []GaSession    `json:"sessions,omitempty"`
	GaReferers       []GaReferer    `json:"referers,omitempty"`
	MCFBasics        []MCFBasic     `json:"mcf,omitempty"`
	GaaWebProperty   GaaWebProperty `json:"web_property,omitempty"`
	GaaWebPropertyID string         `json:"-" gorm:"type:varchar(50)"`
	Name             string         `json:"name,omitempty" gorm:"type:varchar(255)"`
}

type GaaWebProperty struct {
	ID           string       `json:"id" gorm:"type:varchar(50)"`
	Name         string       `json:"name" gorm:"type:varchar(255)"`
	GaaProfiles  []GaaProfile `json:"profiles,omitempty"`
	GaaAccount   GaaAccount   `json:"account,omitempty"`
	GaaAccountID string       `json:"-" gorm:"type:varchar(50)"`
}

type GaaAccount struct {
	ID               string           `json:"id,omitempty" gorm:"type:varchar(50)"`
	GaaWebProperties []GaaWebProperty `json:"web_properties,omitempty"`
	Name             string           `json:"name,omitempty" gorm:"type:varchar(255)"`
	Users            []User           `gorm:"many2many:users_gaa_account"`
	UsersId          []uint64         `gorm:"-"json:"usersID"`
}
