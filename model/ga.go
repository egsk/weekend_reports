package model

import (
	"time"
)

//GaClient Клиент Google Analytics, идентифицирующийся по client_id
type GaClient struct {
	ID         string `json:"id" gorm:"primary_key;type:varchar(60)"`
	GaSessions []GaSession
	ClientID   *uint32
}

//GaSession сессия Google Analytics, привязанная к GaClient
type GaSession struct {
	ID           string  `json:"sid" gorm:"primary_key;type:varchar(60)"`
	Revenue      float64 `json:"revenue"`
	GaClientID   *string `gorm:"type:varchar(60)"`
	GaRefererID  *uint32
	GaaProfileID *string `gorm:"type:varchar(255)"`
	GaClient     GaClient
	GaReferer    GaReferer
	GaaProfile   GaaProfile
	Time         time.Time
}

//GaReferer источник Google Analytics
type GaReferer struct {
	ID uint32 `json:"id" gorm:"primary_key;"`
	/**
	Dimensions block
	 */
	Source    string `json:"source" gorm:"type:varchar(1000)"`     //Источник
	Medium    string `json:"medium" gorm:"type:varchar(1000)"`     //Канал
	Keyword   string `json:"keyword"  gorm:"type:varchar(1000)"`   //Ключевое слово
	AdContent string `json:"ad_content" gorm:"type:varchar(1000)"` //Содержание объявления
	/**
	Metrics block
	 */
	Transactions             uint32  //Количество транзакций
	AdCost                   float64 //Расход на рекламу
	AdClicks                 float64 //Кол-во кликов
	BounceRate               float64 //Процент отказов
	AvgRevenuePerTransaction float64 //Средний чек
	GaSessions               []GaSession
	GaaProfileID             *string `gorm:"type:varchar(50)"`
	MCFWeight                uint32  `gorm:"-"`
	MCFRevenue               float64 `gorm:"-"`
	MCFReducedRevenue        float64 `gorm:"-"`
}
