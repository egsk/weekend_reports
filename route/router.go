package route

import (
	"vst/config"
	"vst/controller"
	"vst/middleware/security"
	"vst/middleware/user"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func Init() *echo.Echo {
	e := echo.New()
	e.Use(middleware.CORS())
	g := e.Group("/api")

	g.Use(middleware.JWT([]byte(config.Get().Security.Secret)))
	g.Use(user.Middleware)
	g.Use(security.Middleware)

	listen(g, e)

	return e
}

func listen(g *echo.Group, e *echo.Echo) {
	e.POST("/login", controller.Login)
	mcfGroup := g.Group("/mcf-report")

	mcfGroup.GET("/list", controller.GetReportsList)
	mcfGroup.GET("/:id", controller.GetMFCByParams)

	mcfGroup.POST("/create", controller.CreateReport)

	aGroup := g.Group("/analytics")
	gaGroup := aGroup.Group("/google")

	gaGroup.GET("/accounts", controller.GetAccounts)

	gaSessionsGroup := gaGroup.Group("/sessions")

	gaSessionsGroup.POST("/collect", controller.CollectSessions)
	gaSessionsGroup.GET("/available/:id", controller.GetAvailableSessionDates)

	userGroup := g.Group("/user")
	userGroup.POST("/add", controller.CreateUser)

}
