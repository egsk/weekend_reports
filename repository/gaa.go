package repository

import (
	"vst/db"
	"vst/model"
)

type GaaProfileRepository struct{}

func (GaaProfileRepository) Find(ID string) *model.GaaProfile {
	view := &model.GaaProfile{}
	db.GetDB().Where("id = ?", ID).Take(view)
	return view
}

type GaaAccountRepository struct{}

func (GaaAccountRepository) FindAllWithRelations() *[]model.GaaAccount {

	var acc []model.GaaAccount

	db.GetDB().Preload("GaaWebProperties").
		Preload("GaaWebProperties.GaaProfiles").Find(&acc)

	return &acc
}

func (GaaAccountRepository) FindWithRelationsByUser(u model.User) *[]model.GaaAccount {
	var acc []model.GaaAccount

	db.GetDB().Preload("GaaAccounts").
		Preload("GaaAccounts.GaaWebProperties").
		Preload("GaaAccounts.GaaWebProperties.GaaProfiles").
		Take(&u)

	acc = u.GaaAccounts

	return &acc
}
