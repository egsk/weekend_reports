package repository

import (
	"vst/db"
	"vst/model"
)

type UserRepository struct{}

func (UserRepository) Find(ID string) *model.User {
	u := model.User{}
	db.GetDB().Where("id = ?", ID).Take(&u)

	return &u
}

func (UserRepository) AppendGaaAccounts(user *model.User) {
	var acc []model.GaaAccount
	for _, id := range user.GaaAccountsId {
		acc = append(acc, model.GaaAccount{ID: id})
	}
	db.GetDB().Model(&user).Association("GaaAccounts").Append(acc)
}

func (UserRepository) LoadGaaAccounts(user *model.User) {
	db.GetDB().Preload("GaaAccounts").Where("id = ?", user.ID).Take(user)
	for i := range user.GaaAccounts {
		user.GaaAccountsId = append(user.GaaAccountsId, user.GaaAccounts[i].ID)
	}
}
