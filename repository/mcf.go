package repository

import (
	"vst/db"
	"vst/model"
)

type MCFReportRepository struct{}

func (MCFReportRepository) FindIncludeRelations(id interface{}) *model.MCFReport {
	report := model.MCFReport{}
	db.GetDB().Where("id = ?", id).
		Preload("GaaProfile").
		Preload("GaaProfile.GaaWebProperty").
		Preload("GaaProfile.GaaWebProperty.GaaAccount").
		Preload("GaaProfile.GaaWebProperty.GaaAccounts.Users").Take(&report)

	return &report
}

func (MCFReportRepository) FindAllIncludeRelations() *[]model.MCFReport {
	var reports []model.MCFReport

	db.GetDB().Preload("GaaProfile").
		Preload("GaaProfile.GaaWebProperty").
		Preload("GaaProfile.GaaWebProperty.GaaAccount").
		Find(&reports)

	return &reports
}

func (MCFReportRepository) FindByUser(u model.User) *[]model.MCFReport {
	var reports []model.MCFReport
	ur := UserRepository{}
	ur.LoadGaaAccounts(&u)
	db.GetDB().Where("GaaProfile.id IN ?", u.GaaAccountsId).Find(&reports)

	return &reports
}

func (MCFReportRepository) Find(id interface{}) *model.MCFReport {
	report := model.MCFReport{}
	db.GetDB().Where("id = ?", id).Take(&report)

	return &report
}

type MCFBasicRepository struct{}

func (MCFBasicRepository) FindAllByMCFReport(report *model.MCFReport) []model.MCFBasic {
	var rows []model.MCFBasic
	db.GetDB().Where("mcf_report_id = ?", report.ID).Find(&rows)
	return rows
}
