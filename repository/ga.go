package repository

import (
	"github.com/jinzhu/gorm"
	"time"
	"vst/db"
	"vst/model"
	"vst/service/analytics/google/native"
)

type GaRefererRepository struct{}

func (GaRefererRepository) FindByFullReferer(source, medium, keyword, adContent string) *model.GaReferer {
	ref := &model.GaReferer{}
	db.GetDB().Where("source = ? AND medium = ? AND keyword = ? and ad_content = ?").Take(ref)

	return ref
}

func (GaRefererRepository) FindByGaaProfile(gaav *model.GaaProfile) []model.GaReferer {
	var ref []model.GaReferer
	db.GetDB().Where("gaa_profile_id = ?", gaav.ID).Find(&ref)

	return ref
}

type GaClientRepository struct{}

func (GaClientRepository) Find(ID string) *model.GaClient {
	c := model.GaClient{}
	db.GetDB().Where("id = ?", ID).Take(&c)

	return &c
}

func (GaClientRepository) FindWithRevenue(params *gan.ReportParams) *[]model.GaClient {
	var c []model.GaClient
	sql := `SELECT ga_clients.id FROM ga_sessions
			LEFT JOIN ga_clients ON ga_sessions.ga_client_id = ga_clients.id
			WHERE ga_sessions.revenue > 0 AND gaa_profile_id = ?
			AND (time::date >= ? AND time::date <= ?);
			`
	rows, err := db.GetDB().Raw(
		sql,
		params.GaaProfile.ID,
		params.DateRanges[0].StartDate,
		params.DateRanges[0].EndDate,
	).Rows()
	if err != nil {
		panic(err)
	}
	var ids []string
	var id string
	for rows.Next() {
		err = rows.Scan(&id)
		if err != nil {
			panic(err)
		}
		ids = append(ids, id)
	}
	db.GetDB().Where("id in (?)", ids).Preload("GaSessions", func(db *gorm.DB) *gorm.DB {
		return db.Order("ga_sessions.time ASC")
	}).Preload("GaSessions.GaReferer").Find(&c)

	return &c
}

//func (GaClientRepository) LoadAssociations

type GaSessionRepository struct{}

func (GaSessionRepository) Find(ID string) *model.GaSession {
	session := model.GaSession{}
	db.GetDB().Where("id = ?", ID).Take(&session)

	return &session
}

func (GaSessionRepository) FindFirstAndLastSessionOfProfile(gp model.GaaProfile) *[2]model.GaSession {
	var res [2]model.GaSession
	db.GetDB().Where("google_analytics_profile_id = ?", gp.ID).
		Order("time").Take(&res[0])
	db.GetDB().Where("google_analytics_profile_id = ?", gp.ID).
		Order("time", true).Take(&res[1])

	return &res
}

func (GaSessionRepository) FindByProfileWithRevenue(v *model.GaaProfile) *[]model.GaSession {
	var s []model.GaSession
	db.GetDB().Where("revenue > 0 AND gaa_profile_id = ?", v.ID).Preload("GaClient").Find(&s)

	return &s
}

func (GaSessionRepository) FindAvailableDaysOfProfile(profile model.GaaProfile) *[]time.Time {

	var t []time.Time
	sql := "SELECT DISTINCT time::date FROM ga_sessions WHERE gaa_profile_id = ? ORDER BY time"

	rows, _ := db.GetDB().Raw(sql, profile.ID).Rows()

	defer rows.Close()

	for rows.Next() {
		var ts string
		rows.Scan(&ts)
		tt, _ := time.Parse("2006-01-02", ts[0:10])
		t = append(t, tt)
	}

	return &t
}
